// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function problem4(inventory){
    if(inventory.length > 0){
        let car_years=[];
        for(index = 0 ; index < inventory.length ; index++){
            car_years.push(inventory[index].car_year);
        }
        return car_years;
    }
    else{
        return "Array is Empty";
    }
}

module.exports = problem4;