// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function problem6(inventory){
    if(inventory.length > 0){
        let interestedCars=[];
        for(index = 0 ; index < inventory.length ; index ++){
            if(inventory[index].car_make == "Audi" || inventory[index].car_make == "BMW"){
                interestedCars.push(JSON.stringify(inventory[index]));
            }
        }
        return interestedCars;
    }
    else{
        return "Array is empty";
    }
}

module.exports = problem6;